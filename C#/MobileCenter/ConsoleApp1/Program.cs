﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ConsoleApp1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using MobileCenter.Data;
    using MobileCenter.Logics;

    /// <summary>
    /// In this class you can find the main menu.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The startup method.
        /// </summary>
        /// <param name="args">asd.</param>
       private static void Main(string[] args)
        {
            bool kilep = false;
            Logic logic = new Logic();

            while (!kilep)
            {
                Console.Clear();
                Console.WriteLine(" 1 Listázás");
                Console.WriteLine(" 2 Beszúrás");
                Console.WriteLine(" 3 Törlés");
                Console.WriteLine(" 4 Módosítás");
                Console.WriteLine(" 5 Vásárlás adatai");
                Console.WriteLine(" 6 Szűrés márkára");
                Console.WriteLine(" 7 Leltár lekérése");
                Console.WriteLine(" 8 Java");
                Console.WriteLine(" 9 Kilépés");
                Console.Write("===========> ");
                int num;
                num = int.Parse(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Melyik táblát szeretnéd kilistázni?\n1: Telefon\n2: Telephely\n3: Vasarlo\n4: Vasarlas");
                        int seged1;
                        seged1 = int.Parse(Console.ReadLine());
                        switch (seged1)
                        {
                            case 1:
                                List<Telefon> telefonLista = logic.ReadTelefon();
                                foreach (var item in telefonLista)
                                {
                                    Console.WriteLine(
                                        "ID: " + item.TelefonID + ", márka: " + item.Marka
                                        + ", típus: " + item.Tipus + ", ár: " + item.Ar.ToString() + "-HUF, telephely: " + item.Tnev);
                                }

                                Console.ReadLine();
                                break;

                            case 2:
                                Console.Clear();
                                List<Telephely> telephelyLista = logic.ReadTelephely();
                                foreach (var item in telephelyLista)
                                {
                                    Console.WriteLine("Telephely neve: " + item.Tnev + ", címe: " + item.Tcim
                                        + ", üzemeletője: " + item.Uzemelteto);
                                }

                                Console.ReadLine();
                                break;

                            case 3:
                                Console.Clear();
                                List<Vasarlo> vasarloLista = logic.ReadVasarlo();
                                foreach (var item in vasarloLista)
                                {
                                    Console.WriteLine("Telefonszám: " + item.Telefonszam + ", neve: "
                                        + item.Vnev + ", születési dátum: " + item.Szul_datum + ", e-mail cím: "
                                        + item.email + ", címe: " + item.Vcim);
                                }

                                Console.ReadLine();
                                break;

                            case 4:
                                Console.Clear();
                                List<Vasarlas> vasarlasLista = logic.ReadVasarlas();
                                foreach (var item in vasarlasLista)
                                {
                                    Console.WriteLine(
                                        "Vásárlás azonosítója: " + item.VasarlasID + ", vevő száma: " +
                                        item.Telefonszam + ", telephely: " + item.Tnev + ", Telefon azonosítója: " + item.TelefonID + ", vásárlás dátuma: " +
                                         item.Vasarlasi_datum);
                                }

                                Console.ReadLine();
                                break;

                            default:
                                Console.WriteLine("Nincs ilyen lehetőség...");
                                break;
                        }

                        break;

                    case 2:
                        Console.Clear();
                        Console.WriteLine("Melyik táblába szeretnél beszúrni?\n1: Telefon\n2: Telephely\n3: Vasarlo\n4: Vasarlas");
                        int seged2;
                        seged2 = int.Parse(Console.ReadLine());
                        switch (seged2)
                        {
                            case 1:
                                Console.WriteLine("Kérem adja meg a telefon adatait:");

                                int telefonID = logic.ReadTelefon().Count() + 1;
                                Console.WriteLine("Márka: ");
                                string marka = Console.ReadLine();
                                Console.WriteLine("Típus: ");
                                string tipus = Console.ReadLine();
                                Console.WriteLine("Ár: ");
                                int ar = int.Parse(Console.ReadLine());
                                Console.WriteLine("Írjon be egyet az alábbi telephelyek közül: ");
                                List<Telephely> telephelyek = logic.ReadTelephely();
                                foreach (var item in telephelyek)
                                {
                                    Console.Write(item.Tnev + ", ");
                                }

                                Console.WriteLine();
                                string telephely = Console.ReadLine();
                                logic.CreateTelefon(telefonID, marka, tipus, ar, telephely);
                                break;

                            case 2:
                                Console.WriteLine("Kérem adja meg a telephely adatait: \nNeve: ");
                                string tnev = Console.ReadLine();
                                Console.WriteLine("Címe: ");
                                string tcim = Console.ReadLine();
                                Console.WriteLine("Üzemeltető: ");
                                string uzemelteto = Console.ReadLine();
                                logic.CreateTelephely(tnev, tcim, uzemelteto);
                                break;

                            case 3:
                                Console.WriteLine("Kérem a vásárló adatait: \nTelefonszám (pl.: 06303132366): ");
                                long telefonszam = long.Parse(Console.ReadLine());
                                Console.WriteLine("Neve: ");
                                string vnev = Console.ReadLine();
                                Console.WriteLine("Születési dátum (ÉÉÉÉ-HH-NN): ");
                                string line = Console.ReadLine();
                                DateTime dt;
                                while (!DateTime.TryParseExact(line, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out dt))
                                {
                                    Console.WriteLine("Nem jó a formátum. Újra.");
                                    line = Console.ReadLine();
                                }

                                Console.WriteLine("E-mail címe: ");
                                string email = Console.ReadLine();
                                Console.WriteLine("Címe: ");
                                string vcim = Console.ReadLine();
                                logic.CreateVasarlo(telefonszam, vnev, dt, email, vcim);
                                break;

                            case 4:

                                List<Vasarlo> vasaLista = logic.ReadVasarlo();
                                foreach (var item in vasaLista)
                                {
                                    Console.WriteLine("Telefonszám: " + item.Telefonszam + ", neve: "
                                        + item.Vnev + ", születési dátum: " + item.Szul_datum + ", e-mail cím: "
                                        + item.email + ", címe: " + item.Vcim);
                                }

                                Console.WriteLine("Vevő telefonszáma: ");
                                long vtelefonszam = long.Parse(Console.ReadLine());
                                List<Telefon> teleLista = logic.ReadTelefon();
                                foreach (var item in teleLista)
                                {
                                    Console.WriteLine(
                                        "ID: " + item.TelefonID + ", márka: " + item.Marka
                                        + ", típus: " + item.Tipus + ", ár: " + item.Ar.ToString() + "-HUF, telephely: " + item.Tnev);
                                }

                                string telep = string.Empty;
                                Console.WriteLine("Telefon azonosító: ");
                                int azon = int.Parse(Console.ReadLine());
                                List<Telefon> teLista = logic.ReadTelefon();
                                foreach (var item in teLista)
                                {
                                    if (item.TelefonID == azon)
                                    {
                                        telep = item.Tnev;
                                        break;
                                    }
                                }

                                Console.WriteLine("Vásárlási dátum (ÉÉÉÉ-HH-NN): ");
                                string sor = Console.ReadLine();
                                DateTime dat;
                                while (!DateTime.TryParseExact(sor, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out dat))
                                {
                                    Console.WriteLine("Nem jó a formátum. Újra.");
                                    sor = Console.ReadLine();
                                }

                                List<Vasarlas> vasarlasok = logic.ReadVasarlas();
                                logic.CreateVasarlas(vasarlasok.Count() + 1, vtelefonszam, telep, azon, dat);
                                break;

                            default:
                                Console.WriteLine("Nincs ilyen lehetőség...");
                                break;
                        }

                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Melyik táblából szertnél törölni?\n1: Telefon\n2: Telephely\n3: Vasarlo\n4: Vasarlas");
                        int seged3;
                        seged3 = int.Parse(Console.ReadLine());
                        switch (seged3)
                        {
                            case 1:
                                Console.Clear();
                                List<Telefon> telefonosLista = logic.ReadTelefon();
                                foreach (var item in telefonosLista)
                                {
                                    Console.WriteLine(
                                        "ID: " + item.TelefonID + ", márka: " + item.Marka
                                        + ", típus: " + item.Tipus + ", ár: " + item.Ar.ToString() + "-HUF, telephely: " + item.Tnev);
                                }

                                Console.WriteLine("A törlendő sor ID-je: ");
                                logic.DeleteTelefon(int.Parse(Console.ReadLine()));
                                break;

                            case 2:
                                Console.Clear();
                                List<Telephely> telephelyesLista = logic.ReadTelephely();
                                foreach (var item in telephelyesLista)
                                {
                                    Console.WriteLine("Telephely neve: " + item.Tnev + ", címe: " + item.Tcim
                                        + ", üzemeletője: " + item.Uzemelteto);
                                }

                                Console.WriteLine("A törlendő telephely neve: ");
                                logic.DeleteTelephely(Console.ReadLine());
                                break;

                            case 3:
                                List<Vasarlo> vasarlosLista = logic.ReadVasarlo();
                                foreach (var item in vasarlosLista)
                                {
                                    Console.WriteLine("Telefonszám: " + item.Telefonszam + ", neve: "
                                        + item.Vnev + ", születési dátum: " + item.Szul_datum + ", e-mail cím: "
                                        + item.email + ", címe: " + item.Vcim);
                                }

                                Console.WriteLine("A törlendő személy telefonszáma: ");
                                logic.DeleteVasarlo(long.Parse(Console.ReadLine()));
                                break;

                            case 4:
                                List<Vasarlas> vasarlasLista = logic.ReadVasarlas();
                                foreach (var item in vasarlasLista)
                                {
                                    Console.WriteLine("ID: " + item.VasarlasID + ", telefonszám: " + item.Telefonszam + ", telephely: " + item.Tnev + ", telefon azonosítója: " + item.TelefonID + ", vásárás dátuma: " + item.Vasarlasi_datum);
                                }

                                Console.WriteLine("A törlendő sor azonosítója: ");
                                logic.DeleteVasarlas(int.Parse(Console.ReadLine()));
                                break;

                            default:
                                Console.WriteLine("Nincs ilyen lehetőség...");
                                break;
                        }

                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Melyik táblát szeretnéd frissíteni?\n1: Telefon\n2: Telephely\n3: Vasarlo");
                        int seged4;
                        seged4 = int.Parse(Console.ReadLine());
                        switch (seged4)
                        {
                            case 1:
                                List<Telefon> telefonLista = logic.ReadTelefon();
                                foreach (var item in telefonLista)
                                {
                                    Console.WriteLine(
                                        "ID: " + item.TelefonID + ", márka: " + item.Marka
                                        + ", típus: " + item.Tipus + ", ár: " + item.Ar.ToString() + "-HUF, telephely: " + item.Tnev);
                                }

                                Console.WriteLine("Írja be a kiválasztott telefon azonosítóját: ");
                                int t_id = int.Parse(Console.ReadLine());
                                Console.WriteLine("A friss adatokat kérem.\nMárka: ");
                                string marka = Console.ReadLine();
                                Console.WriteLine("Típus: ");
                                string tipus = Console.ReadLine();
                                Console.WriteLine("Ár: ");
                                int ar = int.Parse(Console.ReadLine());
                                Console.WriteLine("Írjon be egyet az alábbi telephelyek közül: ");
                                List<Telephely> telephelyek = logic.ReadTelephely();
                                foreach (var item in telephelyek)
                                {
                                    Console.Write(item.Tnev + ", ");
                                }

                                Console.WriteLine();
                                string telephely = Console.ReadLine();
                                logic.UpdateTelefon(t_id, marka, tipus, ar, telephely);

                                break;

                            case 2:
                                List<Telephely> telephelyLista = logic.ReadTelephely();
                                foreach (var item in telephelyLista)
                                {
                                    Console.WriteLine("Telephely neve: " + item.Tnev + ", címe: " + item.Tcim
                                        + ", üzemeletője: " + item.Uzemelteto);
                                }

                                Console.WriteLine("A frissítendő telephely neve: ");
                                string tnev = Console.ReadLine();
                                Console.WriteLine("Új cím: ");
                                string cim = Console.ReadLine();
                                Console.WriteLine("Üzemeltető: ");
                                string uzemelteto = Console.ReadLine();
                                logic.UpdateTelephely(tnev, cim, uzemelteto);

                                break;

                            case 3:
                                List<Vasarlo> vasarloLista = logic.ReadVasarlo();
                                foreach (var item in vasarloLista)
                                {
                                    Console.WriteLine("Telefonszám: " + item.Telefonszam + ", neve: "
                                        + item.Vnev + ", születési dátum: " + item.Szul_datum + ", e-mail cím: "
                                        + item.email + ", címe: " + item.Vcim);
                                }

                                Console.WriteLine("Melyik ügyfél adatait szeretnéd kijavítani?\nTelefonszáma: ");
                                long szam = long.Parse(Console.ReadLine());
                                Console.WriteLine("Kérem az új adatokat!\nNeve: ");
                                string nev = Console.ReadLine();
                                Console.WriteLine("Születési dátum (ÉÉÉÉ-HH-NN): ");
                                string line = Console.ReadLine();
                                DateTime dt;
                                while (!DateTime.TryParseExact(line, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out dt))
                                {
                                    Console.WriteLine("Nem jó a formátum. Újra.");
                                    line = Console.ReadLine();
                                }

                                Console.WriteLine("E-mail cím: ");
                                string email = Console.ReadLine();
                                Console.WriteLine("Címe: ");
                                string vcim = Console.ReadLine();
                                logic.UpdateVasarlo(szam, nev, dt, email, vcim);
                                break;

                            default:
                                Console.WriteLine("Nincs ilyen lehetőség...");
                                break;
                        }

                        break;
                    case 5:
                        Console.Clear();
                        List<Vasarlas> vasarLista = logic.ReadVasarlas();
                        foreach (var item in vasarLista)
                        {
                            Console.WriteLine(
                                "Vásárlás azonosítója: " + item.VasarlasID + ", vevő száma: " +
                                item.Telefonszam + ", telephely: " + item.Tnev + ", Telefon azonosítója: " + item.TelefonID + ", vásárlás dátuma: " +
                                 item.Vasarlasi_datum);
                        }

                        Console.WriteLine("Írja be az azonosítót: ");
                        Console.WriteLine(logic.VasarlasiAdatok(int.Parse(Console.ReadLine())));
                        Console.ReadLine();

                        break;
                    case 6:
                        Console.Clear();
                        Console.Write("Márka: ");
                        List<string> t = logic.MarkaraSzures(Console.ReadLine());
                        for (int i = 0; i < t.Count(); i++)
                        {
                            Console.WriteLine(t[i]);
                        }

                        Console.ReadLine();
                        break;
                    case 7:
                        List<Telephely> telepL = logic.ReadTelephely();
                        string telephelyNeve = null;
                        List<string> nevek = new List<string>();
                        bool kilepes = false;
                        foreach (var item in telepL)
                        {
                            Console.WriteLine("Telephely neve: " + item.Tnev + ", címe: " + item.Tcim
                                + ", üzemeletője: " + item.Uzemelteto);
                            nevek.Add(item.Tnev);
                        }

                        Console.WriteLine("Telephely neve: ");
                        do
                        {
                            telephelyNeve = Console.ReadLine();
                            if (nevek.Contains(telephelyNeve))
                            {
                                kilepes = true;
                            }
                            else
                            {
                                Console.WriteLine("Nincs ilyen!");
                            }
                        }
                        while (!kilepes);

                        nevek = logic.Leltar(telephelyNeve);
                        for (int i = 0; i < nevek.Count(); i++)
                        {
                            Console.WriteLine(nevek[i]);
                        }

                        Console.ReadLine();
                        break;

                    case 8:
                        Console.Clear();

                        XDocument document = XDocument.Load("http://localhost:8080/oenik_prog3_2019_1_g138p4/AdatKuld");
                        var q = from x in document.Descendants("telefon")
                                select x;
                        foreach (var item in q)
                        {
                            Console.Write("ID: " + item.Element("ID").Value);
                            Console.Write(" Márka: " + item.Element("marka").Value);
                            Console.Write(" Típus: " + item.Element("tipus").Value);
                            Console.Write(" Ár: " + item.Element("ar").Value);
                            Console.Write(" Telephely: " + item.Element("tnev").Value + "\n");
                        }

                        Console.ReadLine();
                        break;
                    case 9:
                        Console.Clear();
                        string exit = string.Empty;
                        Console.WriteLine("Biztos ki akarsz lépni?\ni/n");
                        do
                        {
                            exit = Console.ReadLine();
                            if (exit == "i")
                            {
                                kilep = true;
                            }
                            else if (exit == "n")
                            {
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Nincs ilyen!");
                            }
                        }
                        while (exit != "i" && exit != "n");
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
