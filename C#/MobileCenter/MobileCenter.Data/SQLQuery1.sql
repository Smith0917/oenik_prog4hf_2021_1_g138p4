﻿

CREATE TABLE Vasarlo (
Telefonszam BIGINT,
Vnev VARCHAR(32),
Szul_datum DATE,
email VARCHAR(32),
Vcim VARCHAR(32),
CONSTRAINT Vevo_PK PRIMARY KEY(Telefonszam));

CREATE TABLE Telephely (
Tnev VARCHAR(32),
Tcim VARCHAR(32),
Uzemelteto VARCHAR(32),
CONSTRAINT Telep_PK PRIMARY KEY(Tnev));


CREATE TABLE Telefon (
TelefonID INT,
Marka VARCHAR(20),
Tipus VARCHAR(20),
Ar INT,
Tnev VARCHAR(32),
CONSTRAINT Roncs_pk PRIMARY KEY(TelefonID),
CONSTRAINT Telep_fk FOREIGN KEY(Tnev) REFERENCES
Telephely(Tnev));

CREATE TABLE Vasarlas (
VasarlasID INT,
Telefonszam BIGINT,
Tnev VARCHAR(32),
TelefonID INT,
Vasarlasi_datum DATE,
CONSTRAINT Vasarlas_pk PRIMARY KEY(VasarlasID),
CONSTRAINT Vevo_FK FOREIGN KEY(Telefonszam) REFERENCES
Vasarlo(Telefonszam),
CONSTRAINT Telephely_fk FOREIGN KEY(Tnev) REFERENCES
Telephely(Tnev),
CONSTRAINT Roncs_fk FOREIGN KEY(TelefonID) REFERENCES
Telefon(TelefonID));


INSERT INTO Telephely VALUES
('Pesti GSM', '1164 Budapest, Vidámvásár u. 88', 'Acé Levente');

INSERT INTO Telephely VALUES
('Buda GSM', '1225 Budapest, Bentonit u. 2', 'Rozs Dániel');

INSERT INTO Telephely VALUES
('Vidám GSM', '8914 Vasboldogasszony, Ág u. 4', 'Vidám Gabriella');

INSERT INTO Telephely VALUES
('Szlambathely GSM', '9700 Szombathely, Zanati u. 23', 'Gazda Gábor');

INSERT INTO Telephely VALUES
('Tirpák GSM', '4400 Nyíregyháza, Román u. 19', 'Bogyoszló Péter');


INSERT INTO Telefon VALUES
('1', 'Sony', 'Xperia XA1', '50000', 'Buda GSM');

INSERT INTO Telefon VALUES
('2', 'Sony', 'Xperia XZ3', '240000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('3', 'Sony', 'Xperia M5', '40000', 'Szlambathely GSM');

INSERT INTO Telefon VALUES
('4', 'Sony', 'Xperia Z5', '70000', 'Vidám GSM');

INSERT INTO Telefon VALUES
('5', 'Sony', 'Xperia XA2', '90000', 'Tirpák GSM');

INSERT INTO Telefon VALUES
('6', 'Sony', 'Xperia XZ3', '250000', 'Buda GSM');

INSERT INTO Telefon VALUES
('7', 'Samsung', 'Galaxy S9+', '190000', 'Pesti GSM'); 

INSERT INTO Telefon VALUES
('8', 'Samsung', 'Galaxy A8', '100000', 'Szlambathely GSM');

INSERT INTO Telefon VALUES
('9', 'Samsung', 'Galaxy Note9', '230000', 'Vidám GSM');

INSERT INTO Telefon VALUES
('10', 'Samsung', 'Galaxy A8', '85000', 'Tirpák GSM');

INSERT INTO Telefon VALUES
('11', 'Samsung', 'Galaxy S8', '125000', 'Buda GSM'); 

INSERT INTO Telefon VALUES
('12', 'Samsung', 'Galaxy A7', '90000', 'Pesti GSM'); 

INSERT INTO Telefon VALUES
('13', 'iPhone', '7', '185000', 'Szlambathely GSM');

INSERT INTO Telefon VALUES
('14', 'iPhone', '6s', '159000', 'Vidám GSM');

INSERT INTO Telefon VALUES
('15', 'iPhone', '7', '199000', 'Tirpák GSM');

INSERT INTO Telefon VALUES
('16', 'iPhone', '8', '190000', 'Buda GSM');

INSERT INTO Telefon VALUES
('17', 'iPhone', 'XR', '315000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('18', 'iPhone', 'X', '275000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('19', 'iPhone', 'X', '290000', 'Buda GSM'); 

INSERT INTO Telefon VALUES
('20', 'Huawei', 'P20 Pro', '200000', 'Pesti GSM'); 

INSERT INTO Telefon VALUES
('21', 'Huawei', 'P20 Lite', '95000', 'Szlambathely GSM');

INSERT INTO Telefon VALUES
('22', 'Huawei', 'P20', '150000', 'Vidám GSM');

INSERT INTO Telefon VALUES
('23', 'Huawei', 'P20 Lite', '90000', 'Tirpák GSM');

INSERT INTO Telefon VALUES
('24', 'Huawei', 'P20', '140000', 'Buda GSM');

INSERT INTO Telefon VALUES
('25', 'Huawei', 'P20', '140000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('26', 'Honor', '10', '120000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('27', 'Honor', '10', '115000', 'Buda GSM');

INSERT INTO Telefon VALUES
('28', 'Honor', '10', '120000', 'Szlambathely GSM');

INSERT INTO Telefon VALUES
('29', 'OnePlus', '6', '170000', 'Vidám GSM');

INSERT INTO Telefon VALUES
('30', 'OnePlus', '5T', '165000', 'Tirpák GSM');

INSERT INTO Telefon VALUES
('31', 'HTC', 'U play', '55000', 'Buda GSM');

INSERT INTO Telefon VALUES
('32', 'HTC', 'U Ultra', '75000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('33', 'HTC', 'U11', '120000', 'Vidám GSM');

INSERT INTO Telefon VALUES
('34', 'HTC', 'U12+', '230000', 'Buda GSM');

INSERT INTO Telefon VALUES
('35', 'Caterpillar', 'S60', '135000', 'Szlambathely GSM');

INSERT INTO Telefon VALUES
('36', 'Caterpillar', 'S61', '235000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('37', 'Nokia', '230', '18000', 'Buda GSM');

INSERT INTO Telefon VALUES
('38', 'Nokia', '8', '90000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('39', 'Nokia', '6.1', '80000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('40', 'Nokia', '3310', '15000', 'Buda GSM');

INSERT INTO Telefon VALUES
('41', 'Nokia', '7 Plus', '100000', 'Szlambathely GSM');

INSERT INTO Telefon VALUES
('42', 'Xiaomi', 'Mi A2', '55000', 'Vidám GSM');

INSERT INTO Telefon VALUES
('43', 'Xiaomi', 'Mi A6', '65000', 'Tirpák GSM');

INSERT INTO Telefon VALUES
('44', 'Xiaomi', 'Mi 8', '135000', 'Buda GSM');

INSERT INTO Telefon VALUES
('45', 'Xiaomi', 'Redmi 6', '65000', 'Pesti GSM');

INSERT INTO Telefon VALUES
('46', 'Motorola', 'Moto G6', '70000', 'Vidám GSM');

INSERT INTO Telefon VALUES
('47', 'Motorola', 'Moto G5', '40000', 'Buda GSM');

INSERT INTO Telefon VALUES
('48', 'Oppo', 'A57', '70000', 'Szlambathely GSM');

INSERT INTO Telefon VALUES
('49', 'Oppo', 'R11', '60000', 'Pesti GSM');

INSERT INTO Vasarlo VALUES
('06303879301', 'Dúr Károly', '1995-11-23', 'Durr@gmail.com', 'Budapest');

INSERT INTO Vasarlo VALUES
('06303879302', 'Hollós Endre', '1969-12-05', 'EndreBa@gmail.com', 'Budapest');

INSERT INTO Vasarlo VALUES
('06303879303', 'Miklósi Márk','1993-10-19', 'Muffhunterz@gmail.com', 'Zalaegerszeg');

INSERT INTO Vasarlo VALUES
('06303879304', 'Németh Lajos', '1999-07-01', 'Laller2@gmail.com', 'Szombathely');

INSERT INTO Vasarlo VALUES
('06303879305', 'Marcz Levente','1997-06-13', 'Arny@gmail.com', 'Nyíregyháza');

INSERT INTO Vasarlo VALUES
('06303879306', 'Varga István','1985-11-05', 'Istike@gmail.com', 'Budapest');


INSERT INTO Vasarlas VALUES
('0001', '06303879301', 'Pesti GSM', '1', '2018-10-21');


INSERT INTO Vasarlas VALUES
('0002', '06303879301', 'Buda GSM', '2', '2018-09-17');

INSERT INTO Vasarlas VALUES
('0003', '06303879302', 'Buda GSM', '3','2019-08-19');

INSERT INTO Vasarlas VALUES
('0004', '06303879303', 'Vidám GSM', '4','2017-05-29');

INSERT INTO Vasarlas VALUES
('0005', '06303879304', 'Szlambathely GSM', '5', '2018-07-27');

INSERT INTO Vasarlas VALUES
('0006', '06303879305', 'Tirpák GSM', '6','2018-01-20');
