﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MobileCenter.Logics
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MobileCenter.Data;
    using MobileCenter.Repository;

    /// <summary>
    /// This class implements methods from ILogic interface.
    /// </summary>
    public class Logic : ILogic
    {
        private readonly Repo repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Logic class's constructor with parameter.
        /// </summary>
        /// <param name="repo">A repository object.</param>
        public Logic(Repo repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Parameterless constructor of Logic class.
        /// </summary>
        public Logic()
        {
            this.repo = new Repo();
        }

        /// <summary>
        /// To create a phone.
        /// </summary>
        /// <param name="t_id">Telefon's ID.</param>
        /// <param name="marka">Telefon's brand.</param>
        /// <param name="tipus">Telefon's type.</param>
        /// <param name="ar">Telefon's cost.</param>
        /// <param name="tnev">Name of the department, which this particular model is kept.</param>
        public void CreateTelefon(int t_id, string marka, string tipus, int ar, string tnev)
        {
            Telefon telefon = new Telefon
            {
                TelefonID = t_id,
                Marka = marka,
                Tipus = tipus,
                Ar = ar,
                Tnev = tnev,
            };
            this.repo.CreateTelefon(telefon);
        }

        /// <summary>
        /// To create a department.
        /// </summary>
        /// <param name="tnev">Department's name.</param>
        /// <param name="tcim">Department's address.</param>
        /// <param name="uzemelteto">Department's operator.</param>
        public void CreateTelephely(string tnev, string tcim, string uzemelteto)
        {
            Telephely telephely = new Telephely
            {
                Tnev = tnev,
                Tcim = tcim,
                Uzemelteto = uzemelteto,
            };
            this.repo.CreateTelephely(telephely);
        }

        /// <summary>
        /// To create a custumer.
        /// </summary>
        /// <param name="telefonszam">Custumer's phone number.</param>
        /// <param name="vnev">Custumer's name.</param>
        /// <param name="datum">Custumer's birthdate.</param>
        /// <param name="email">Custumer's e-mail address.</param>
        /// <param name="vcim">Custumer's address.</param>
        public void CreateVasarlo(long telefonszam, string vnev, DateTime datum, string email, string vcim)
        {
            Vasarlo vasarlo = new Vasarlo
            {
                Telefonszam = telefonszam,
                Vnev = vnev,
                Szul_datum = datum,
                email = email,
                Vcim = vcim,
            };
            this.repo.CreateVasarlo(vasarlo);
        }

        /// <summary>
        /// To create a purchase.
        /// </summary>
        /// <param name="vasarlasID">Purchase's ID.</param>
        /// <param name="telefonszam">Custumer's phone number (foreign key to Vasarlo).</param>
        /// <param name="tnev">Department's name (foreign key to Telephely).</param>
        /// <param name="telefonID">Phone's ID (foreign key to Telefon).</param>
        /// <param name="datum">Date of the purchase.</param>
        public void CreateVasarlas(int vasarlasID, long telefonszam, string tnev, int telefonID, DateTime datum)
        {
            Vasarlas vasarlas = new Vasarlas
            {
                VasarlasID = vasarlasID,
                Telefonszam = telefonszam,
                Tnev = tnev,
                TelefonID = telefonID,
                Vasarlasi_datum = datum,
            };
            this.repo.CreateVasarlas(vasarlas);
        }

        /// <summary>
        /// Delete a phone.
        /// </summary>
        /// <param name="t_id">ID of the desired phone to delete.</param>
        public void DeleteTelefon(int t_id)
        {
            Telefon telefon = this.repo.Database.Telefons.Single(x => x.TelefonID == t_id);
            this.repo.DeleteTelefon(telefon);
        }

        /// <summary>
        /// Delete a department.
        /// </summary>
        /// <param name="tnev">department's name.</param>
        public void DeleteTelephely(string tnev)
        {
            Telephely telephely = this.repo.Database.Telephelies.Single(x => x.Tnev == tnev);
            this.repo.DeleteTelephely(telephely);
        }

        /// <summary>
        /// Delete a customer.
        /// </summary>
        /// <param name="telefonszam">Customer's phone number.</param>
        public void DeleteVasarlo(long telefonszam)
        {
            Vasarlo vasarlo = this.repo.Database.Vasarloes.Single(x => x.Telefonszam == telefonszam);
            this.repo.DeleteVasarlo(vasarlo);
        }

        /// <summary>
        /// Delete a purchase.
        /// </summary>
        /// <param name="id">Purchase's ID.</param>
        public void DeleteVasarlas(int id)
        {
            Vasarlas vasarlas = this.repo.Database.Vasarlas.Single(x => x.VasarlasID == id);
            this.repo.DeleteVasarlas(vasarlas);
        }

        /// <summary>
        /// Lists the rows of Telefon table.
        /// </summary>
        /// <returns>A list of Telefons.</returns>
        public List<Telefon> ReadTelefon()
        {
            return this.repo.ReadTelefon();
        }

        /// <summary>
        /// Lists the rows of Telephely table.
        /// </summary>
        /// <returns>A list of Telephely.</returns>
        public List<Telephely> ReadTelephely()
        {
            return this.repo.ReadTelephely();
        }

        /// <summary>
        /// Lists the rows of Vasarlo table.
        /// </summary>
        /// <returns>A list of Vasarlo.</returns>
        public List<Vasarlo> ReadVasarlo()
        {
            return this.repo.ReadVasarlo();
        }

        /// <summary>
        /// Lists the rows of Vasarlas table.
        /// </summary>
        /// <returns>A list of Vasarlas.</returns>
        public List<Vasarlas> ReadVasarlas()
        {
            return this.repo.ReadVasarlas();
        }

        /// <summary>
        /// Update a particular element of Telefon table.
        /// </summary>
        /// <param name="t_id">ID of phone which will be updated.</param>
        /// <param name="ujMarka">New brand.</param>
        /// <param name="ujTipus">New type.</param>
        /// <param name="ujAr">New cost.</param>
        /// <param name="ujTnev">Department's name.</param>
        public void UpdateTelefon(int t_id, string ujMarka, string ujTipus, int ujAr, string ujTnev)
        {
            Telefon telefon1 = this.repo.Database.Telefons.Single(x => x.TelefonID == t_id);
            Telefon telefon2 = new Telefon
            {
                TelefonID = t_id,
                Marka = ujMarka,
                Tipus = ujTipus,
                Ar = ujAr,
                Tnev = ujTnev,
            };
            this.repo.UpdateTelefon(telefon1, telefon2);
        }

        /// <summary>
        /// Update a particular element of Telephely table.
        /// </summary>
        /// <param name="tnev">Name of the department which will be updated.</param>
        /// <param name="ujTcim">New address.</param>
        /// <param name="ujUzemelteto">New operator.</param>
        public void UpdateTelephely(string tnev, string ujTcim, string ujUzemelteto)
        {
            Telephely telephely1 = this.repo.Database.Telephelies.Single(x => x.Tnev == tnev);
            Telephely telephely2 = new Telephely
            {
                Tcim = ujTcim,
                Uzemelteto = ujUzemelteto,
            };
            this.repo.UpdateTelephely(telephely1, telephely2);
        }

        /// <summary>
        /// Update a particular element of Vasarlo table.
        /// </summary>
        /// <param name="telefonszam">Phone number of customer which will be updated.</param>
        /// <param name="ujNev">New name.</param>
        /// <param name="ujDatum">New birthdate.</param>
        /// <param name="ujEmail">New e-mail address.</param>
        /// <param name="ujVcim">New address.</param>
        public void UpdateVasarlo(long telefonszam, string ujNev, DateTime ujDatum, string ujEmail, string ujVcim)
        {
            Vasarlo vasarlo1 = this.repo.Database.Vasarloes.Single(x => x.Telefonszam == telefonszam);
            Vasarlo vasarlo2 = new Vasarlo
            {
                Vnev = ujNev,
                Szul_datum = ujDatum,
                email = ujEmail,
                Vcim = ujVcim,
            };
            this.repo.UpdateVasarlo(vasarlo1, vasarlo2);
        }

        /// <summary>
        /// Lists the details of a particular purchase.
        /// </summary>
        /// <param name="iD">Purchase's ID.</param>
        /// <returns>Details of a Vasarlas.</returns>
        public string VasarlasiAdatok(int iD)
        {
            List<Vasarlas> vasarlasLista = this.ReadVasarlas();
            List<Vasarlo> vasarloLista = this.ReadVasarlo();
            List<Telefon> telefonok = this.ReadTelefon();
            Vasarlas v1 = null;
            string nev = string.Empty;
            string model = string.Empty;
            int ar = 0;

            foreach (var item in vasarlasLista)
            {
                if (item.VasarlasID == iD)
                {
                    v1 = item;
                }
            }

            foreach (var item in vasarloLista)
            {
                if (item.Telefonszam == v1.Telefonszam)
                {
                    nev = item.Vnev;
                }
            }

            foreach (var item in telefonok)
            {
                if (item.TelefonID == v1.TelefonID)
                {
                    model = item.Marka + " " + item.Tipus;
                    ar = (int)item.Ar;
                }
            }

            return nev + " vásárolt " + model + " készüléket " + ar.ToString() + "-ért.";
        }

        /// <summary>
        /// Lists the phones if a particular brand.
        /// </summary>
        /// <param name="marka">Brand's name.</param>
        /// <returns>Products of one brand.</returns>
        public List<string> MarkaraSzures(string marka)
        {
            List<Telefon> telefonok = this.ReadTelefon();
            List<string> t = new List<string>();

            foreach (var item in telefonok)
            {
                if (item.Marka == marka)
                {
                    t.Add("ID: " + item.TelefonID + " Modell: " + item.Marka + " " + item.Tipus +
                       " Ára: " + item.Ar + " itt: " + item.Tnev);
                }
            }

            return t;
        }

        /// <summary>
        /// Lists the phones stored in a particular department.
        /// </summary>
        /// <param name="nev">Department's name.</param>
        /// <returns>Telefons in one Telephely.</returns>
        public List<string> Leltar(string nev)
        {
            Dictionary<int, string> whiteSpace = new Dictionary<int, string>();

            int whiteSpaceLength = 15;
            for (int i = 0; i < whiteSpaceLength; i++)
            {
                string value = string.Empty;
                for (int j = 0; j < i + 1; j++)
                {
                    value += " ";
                }

                whiteSpace.Add(i + 1, value);
            }

            List<string> t = new List<string>();
            List<Telefon> temp = this.ReadTelefon();
            t.Add("Azonosító      Márka          Típus          Ár             ");

            foreach (var item in temp)
            {
                if (item.Tnev == nev)
                {
                    t.Add(item.TelefonID + whiteSpace[whiteSpaceLength - item.TelefonID.ToString().Length] + item.Marka + whiteSpace[whiteSpaceLength - item.Marka.Length] + item.Tipus + whiteSpace[whiteSpaceLength - item.Tipus.Length] + item.Ar);
                }
            }

            return t;
        }
    }
}
