﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MobileCenter.Logics
{
    using System;
    using System.Collections.Generic;
    using MobileCenter.Data;

    /// <summary>
    /// This is the interface which will be implemented Logic class.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// To create a phone.
        /// </summary>
        /// <param name="t_id">Telefon's ID.</param>
        /// <param name="marka">Telefon's brand.</param>
        /// <param name="tipus">Telefon's type.</param>
        /// <param name="ar">Telefon's cost.</param>
        /// <param name="tnev">Name of the department, which this particular model is kept.</param>
        void CreateTelefon(int t_id, string marka, string tipus, int ar, string tnev);

        /// <summary>
        /// To create a department.
        /// </summary>
        /// <param name="tnev">Department's name.</param>
        /// <param name="tcim">Department's address.</param>
        /// <param name="uzemelteto">Department's operator.</param>
        void CreateTelephely(string tnev, string tcim, string uzemelteto);

        /// <summary>
        /// To create a custumer.
        /// </summary>
        /// <param name="telefonszam">Custumer's phone number.</param>
        /// <param name="vnev">Custumer's name.</param>
        /// <param name="datum">Custumer's birthdate.</param>
        /// <param name="email">Custumer's e-mail address.</param>
        /// <param name="vcim">Custumer's address.</param>
        void CreateVasarlo(long telefonszam, string vnev, DateTime datum, string email, string vcim);

        /// <summary>
        /// To create a purchase.
        /// </summary>
        /// <param name="vasarlasID">Purchase's ID.</param>
        /// <param name="telefonszam">Custumer's phone number (foreign key to Vasarlo).</param>
        /// <param name="tnev">Department's name (foreign key to Telephely).</param>
        /// <param name="telefonID">Phone's ID (foreign key to Telefon).</param>
        /// <param name="datum">Date of the purchase.</param>
        void CreateVasarlas(int vasarlasID, long telefonszam, string tnev, int telefonID, DateTime datum);

        /// <summary>
        /// Delete a phone.
        /// </summary>
        /// <param name="t_id">ID of the desired phone to delete.</param>
        void DeleteTelefon(int t_id);

        /// <summary>
        /// Delete a department.
        /// </summary>
        /// <param name="tnev">department's name.</param>
        void DeleteTelephely(string tnev);

        /// <summary>
        /// Delete a customer.
        /// </summary>
        /// <param name="telefonszam">Customer's phone number.</param>
        void DeleteVasarlo(long telefonszam);

        /// <summary>
        /// Delete a purchase.
        /// </summary>
        /// <param name="id">Purchase's ID.</param>
        void DeleteVasarlas(int id);

        /// <summary>
        /// Lists the rows of Telefon table.
        /// </summary>
        /// <returns>List of Telefons.</returns>
        List<Telefon> ReadTelefon();

        /// <summary>
        /// Lists the rows of Telephely table.
        /// </summary>
        /// <returns>List of Telephelys.</returns>
        List<Telephely> ReadTelephely();

        /// <summary>
        /// Lists the rows of Vasarlo table.
        /// </summary>
        /// <returns>List of Vasarlos.</returns>
        List<Vasarlo> ReadVasarlo();

        /// <summary>
        /// Lists the rows of Vasarlas table.
        /// </summary>
        /// <returns>List of Vasarlas.</returns>
        List<Vasarlas> ReadVasarlas();

        /// <summary>
        /// Update a particular element of Telefon table.
        /// </summary>
        /// <param name="t_id">ID of phone which will be updated.</param>
        /// <param name="ujMarka">New brand.</param>
        /// <param name="ujTipus">New type.</param>
        /// <param name="ujAr">New cost.</param>
        /// <param name="ujTnev">Department's name.</param>
        void UpdateTelefon(int t_id, string ujMarka, string ujTipus, int ujAr, string ujTnev);

        /// <summary>
        /// Update a particular element of Telephely table.
        /// </summary>
        /// <param name="tnev">Name of the department which will be updated.</param>
        /// <param name="ujTcim">New address.</param>
        /// <param name="ujUzemelteto">New operator.</param>
        void UpdateTelephely(string tnev, string ujTcim, string ujUzemelteto);

        /// <summary>
        /// Update a particular element of Vasarlo table.
        /// </summary>
        /// <param name="telefonszam">Phone number of customer which will be updated.</param>
        /// <param name="ujNev">New name.</param>
        /// <param name="ujDatum">New birthdate.</param>
        /// <param name="ujEmail">New e-mail address.</param>
        /// <param name="ujVcim">New address.</param>
        void UpdateVasarlo(long telefonszam, string ujNev, DateTime ujDatum, string ujEmail, string ujVcim);

        /// <summary>
        /// Lists the details of a particular purchase.
        /// </summary>
        /// <param name="iD">Purchase's ID.</param>
        /// <returns>The details of a Vasarlas.</returns>
        string VasarlasiAdatok(int iD);

        /// <summary>
        /// Lists the phones if a particular brand.
        /// </summary>
        /// <param name="marka">Brand's name.</param>
        /// <returns>Telefons of one brand..</returns>
        List<string> MarkaraSzures(string marka);

        /// <summary>
        /// Lists the phones stored in a particular department.
        /// </summary>
        /// <param name="nev">Department's name.</param>
        /// <returns>The objects kept within one Telephely.</returns>
        List<string> Leltar(string nev);
    }
}
