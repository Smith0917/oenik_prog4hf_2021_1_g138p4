﻿// <copyright file="IRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MobileCenter.Repository
{
    using System.Collections.Generic;
    using MobileCenter.Data;

    /// <summary>
    /// This is an interface which will be implemented by Repo class.
    /// </summary>
    internal interface IRepo
    {/// <summary>
     /// Adds a telefon object to Telefon table.
     /// </summary>
     /// <param name="telefon">A telefon object.</param>
        void CreateTelefon(Telefon telefon);

        /// <summary>
        /// Adds a telephely object to Telephely table.
        /// </summary>
        /// <param name="telephely">Telephely object.</param>
        void CreateTelephely(Telephely telephely);

        /// <summary>
        /// Adds a vasarlo object to Telefon table.
        /// </summary>
        /// <param name="vasarlo">Vasarlo object.</param>
        void CreateVasarlo(Vasarlo vasarlo);

        /// <summary>
        /// Adds a new object to Vasarlas table.
        /// </summary>
        /// <param name="vasarlas">Vasarlas object.</param>
        void CreateVasarlas(Vasarlas vasarlas);

        /// <summary>
        /// Removes a record from telefon table.
        /// </summary>
        /// <param name="telefon">telefon object.</param>
        void DeleteTelefon(Telefon telefon);

        /// <summary>
        /// removes a record from telephely table.
        /// </summary>
        /// <param name="telephely">telephely object.</param>
        void DeleteTelephely(Telephely telephely);

        /// <summary>
        /// removes an object from vasarlo table.
        /// </summary>
        /// <param name="vasarlo">vasarlo object.</param>
        void DeleteVasarlo(Vasarlo vasarlo);

        /// <summary>
        /// removes a record from vasarlas table.
        /// </summary>
        /// <param name="vasarlas">vasarlas object.</param>
        void DeleteVasarlas(Vasarlas vasarlas);

        /// <summary>
        /// Reads all records from telefon table.
        /// </summary>
        /// <returns>a list of all telefon objects.</returns>
        List<Telefon> ReadTelefon();

        /// <summary>
        /// Reads all records from telephely table.
        /// </summary>
        /// <returns>list of all telephely objects.</returns>
        List<Telephely> ReadTelephely();

        /// <summary>
        /// reads all records from vasarlo table.
        /// </summary>
        /// <returns>a list of all vasarlos.</returns>
        List<Vasarlo> ReadVasarlo();

        /// <summary>
        /// reads all vasarlas objects.
        /// </summary>
        /// <returns>list of all the vasarlas objects.</returns>
        List<Vasarlas> ReadVasarlas();

        /// <summary>
        /// replaces a record in telefon table.
        /// </summary>
        /// <param name="t1">old.</param>
        /// <param name="t2">new.</param>
        void UpdateTelefon(Telefon t1, Telefon t2);

        /// <summary>
        /// replaces a record in telephely table.
        /// </summary>
        /// <param name="t1">old.</param>
        /// <param name="t2">new.</param>
        void UpdateTelephely(Telephely t1, Telephely t2);

        /// <summary>
        /// replaces a record in vasarlo table.
        /// </summary>
        /// <param name="v1">old.</param>
        /// <param name="v2">new.</param>
        void UpdateVasarlo(Vasarlo v1, Vasarlo v2);
    }
}
