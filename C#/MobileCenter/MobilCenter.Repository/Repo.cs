﻿// <copyright file="Repo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MobileCenter.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using MobileCenter.Data;

    /// <summary>
    /// This class implements the IRepository interface.
    /// </summary>
    public class Repo : IRepo
    {
        private MobileCenterDatabaseEntities database;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repo"/> class.
        /// Repository's parameterless constructor.
        /// </summary>
        public Repo()
        {
            this.database = new MobileCenterDatabaseEntities();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repo"/> class.
        /// </summary>
        /// <param name="database">Constructor's parameter.</param>
        public Repo(MobileCenterDatabaseEntities database)
        {
            this.database = database;
        }

        /// <summary>
        /// Gets database's readable property.
        /// </summary>
        public MobileCenterDatabaseEntities Database
        {
            get
            {
                return this.database;
            }
        }

        /// <summary>
        /// To create a phone.
        /// </summary>
        /// <param name="telefon">Telefon object.</param>
        public void CreateTelefon(Telefon telefon)
        {
            this.Database.Telefons.Add(telefon);
            this.Database.SaveChanges();
        }

        /// <summary>
        /// To create a department.
        /// </summary>
        /// <param name="telephely">Telephely object.</param>
        public void CreateTelephely(Telephely telephely)
        {
            this.Database.Telephelies.Add(telephely);
            this.Database.SaveChanges();
        }

        /// <summary>
        /// To create a customer.
        /// </summary>
        /// <param name="vasarlo">Vasarlo object.</param>
        public void CreateVasarlo(Vasarlo vasarlo)
        {
            this.Database.Vasarloes.Add(vasarlo);
            this.Database.SaveChanges();
        }

        /// <summary>
        /// To create a purchase.
        /// </summary>
        /// <param name="vasarlas">Vasarlas object.</param>
        public void CreateVasarlas(Vasarlas vasarlas)
        {
            this.Database.Vasarlas.Add(vasarlas);
            this.Database.SaveChanges();
        }

        /// <summary>
        /// To delete a department.
        /// </summary>
        /// <param name="telephely">Telephely object.</param>
        public void DeleteTelephely(Telephely telephely)
        {
            this.Database.Telephelies.Remove(telephely);
            this.Database.SaveChanges();
        }

        /// <summary>
        /// To delete a phone.
        /// </summary>
        /// <param name="telefon">Telefon object.</param>
        public void DeleteTelefon(Telefon telefon)
        {
            this.Database.Telefons.Remove(telefon);
            this.Database.SaveChanges();
        }

        /// <summary>
        /// To delete a customer.
        /// </summary>
        /// <param name="vasarlo">Vasarlo object.</param>
        public void DeleteVasarlo(Vasarlo vasarlo)
        {
            this.Database.Vasarloes.Remove(vasarlo);
            this.Database.SaveChanges();
        }

        /// <summary>
        /// To delete a purchase.
        /// </summary>
        /// <param name="vasarlas">Vasarlas object.</param>
        public void DeleteVasarlas(Vasarlas vasarlas)
        {
            this.Database.Vasarlas.Remove(vasarlas);
            this.Database.SaveChanges();
        }

        /// <summary>
        /// Lists all phones.
        /// </summary>
        /// <returns>A list of all Telefon objects.</returns>
        public List<Telefon> ReadTelefon()
        {
            return this.Database.Telefons.ToList();
        }

        /// <summary>
        /// Lists all departments.
        /// </summary>
        /// <returns>A list of all Telephely objects.</returns>
        public List<Telephely> ReadTelephely()
        {
            return this.Database.Telephelies.ToList();
        }

        /// <summary>
        /// Lists all customers.
        /// </summary>
        /// <returns>A list of all Vasarlo objects.</returns>
        public List<Vasarlo> ReadVasarlo()
        {
            return this.Database.Vasarloes.ToList();
        }

        /// <summary>
        /// Lists all purchases.
        /// </summary>
        /// <returns>A list of all Vasarlas objects.</returns>
        public List<Vasarlas> ReadVasarlas()
        {
            return this.Database.Vasarlas.ToList();
        }

        /// <summary>
        /// To update a particular element of Telefon table.
        /// </summary>
        /// <param name="t1">Phone to update.</param>
        /// <param name="t2">New phone.</param>
        public void UpdateTelefon(Telefon t1, Telefon t2)
        {
            t1.Marka = t2.Marka;
            t1.Tipus = t2.Tipus;
            t1.Ar = t2.Ar;
            t1.Tnev = t2.Tnev;
            this.database.SaveChanges();
        }

        /// <summary>
        /// To update a particular elemebt of Telephely table.
        /// </summary>
        /// <param name="t1">Old department.</param>
        /// <param name="t2">New department.</param>
        public void UpdateTelephely(Telephely t1, Telephely t2)
        {
            t1.Tcim = t2.Tcim;
            t1.Uzemelteto = t2.Uzemelteto;
            this.database.SaveChanges();
        }

        /// <summary>
        /// To update a particular element of Vasarlo table.
        /// </summary>
        /// <param name="v1">Old customer.</param>
        /// <param name="v2">New customer.</param>
        public void UpdateVasarlo(Vasarlo v1, Vasarlo v2)
        {
            v1.Vnev = v2.Vnev;
            v1.Szul_datum = v2.Szul_datum;
            v1.email = v2.email;
            v1.Vcim = v2.Vcim;
            this.database.SaveChanges();
        }
    }
}
