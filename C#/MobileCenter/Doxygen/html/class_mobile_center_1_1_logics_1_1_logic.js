var class_mobile_center_1_1_logics_1_1_logic =
[
    [ "Logic", "class_mobile_center_1_1_logics_1_1_logic.html#a68711eae07cf165afc26c956aafe5de2", null ],
    [ "Logic", "class_mobile_center_1_1_logics_1_1_logic.html#ad21f940151579a859d744d1c1892f9ca", null ],
    [ "CreateTelefon", "class_mobile_center_1_1_logics_1_1_logic.html#adb3ccbf0a5f806d70125bd80e0851529", null ],
    [ "CreateTelephely", "class_mobile_center_1_1_logics_1_1_logic.html#a995315c0628778149cd1eed6ba77e2d5", null ],
    [ "CreateVasarlas", "class_mobile_center_1_1_logics_1_1_logic.html#a72b8e3a1536728b2ff1e5500a9f01b0d", null ],
    [ "CreateVasarlo", "class_mobile_center_1_1_logics_1_1_logic.html#a0192cd81a429d108f7b663437b172bfe", null ],
    [ "DeleteTelefon", "class_mobile_center_1_1_logics_1_1_logic.html#ab2cb1fbad180b679d6763b8d85377312", null ],
    [ "DeleteTelephely", "class_mobile_center_1_1_logics_1_1_logic.html#a3ca198d9cb4b006f4eccdd1691599d2f", null ],
    [ "DeleteVasarlas", "class_mobile_center_1_1_logics_1_1_logic.html#a580c57d3aaa4e630baeb7f666891a240", null ],
    [ "DeleteVasarlo", "class_mobile_center_1_1_logics_1_1_logic.html#a65397993e30d5cdc565a7e12a46a904b", null ],
    [ "Leltar", "class_mobile_center_1_1_logics_1_1_logic.html#aaa0fcdfd539186ae25282972308addba", null ],
    [ "MarkaraSzures", "class_mobile_center_1_1_logics_1_1_logic.html#ae1ec21c41e6801b8f37f2e7d0733afbd", null ],
    [ "ReadTelefon", "class_mobile_center_1_1_logics_1_1_logic.html#a7fcb9ad1216b756c815e6854f5b0ac81", null ],
    [ "ReadTelephely", "class_mobile_center_1_1_logics_1_1_logic.html#aec19f7cdd88ed747df03fbfc15eb9780", null ],
    [ "ReadVasarlas", "class_mobile_center_1_1_logics_1_1_logic.html#aaa55584b7af9b02dc37d8f2734f75f45", null ],
    [ "ReadVasarlo", "class_mobile_center_1_1_logics_1_1_logic.html#ac368ad43f8aecc79fdd34a807db37d7f", null ],
    [ "UpdateTelefon", "class_mobile_center_1_1_logics_1_1_logic.html#a91ca2776c26b4f17553a9ade7d1a4881", null ],
    [ "UpdateTelephely", "class_mobile_center_1_1_logics_1_1_logic.html#ad4741ef2ce1f007b0eb5b168ec3056ac", null ],
    [ "UpdateVasarlo", "class_mobile_center_1_1_logics_1_1_logic.html#a86a640c6b79aac3533d3297fee5c18ba", null ],
    [ "VasarlasiAdatok", "class_mobile_center_1_1_logics_1_1_logic.html#acce95664b03e8060633e6725b28ca0ef", null ]
];