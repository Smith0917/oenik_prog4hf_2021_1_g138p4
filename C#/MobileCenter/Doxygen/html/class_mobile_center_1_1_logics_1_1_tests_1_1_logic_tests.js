var class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests =
[
    [ "TestCreateTelefon", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#afd9abba7ebd8205dc123b3380ae84257", null ],
    [ "TestCreateTelephely", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#aad45f9d0f4fd8623aa1a12ab012e8f42", null ],
    [ "TestCreateVasarlas", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a7cb69b46b3af83979925ac25d3886216", null ],
    [ "TestCreateVasarlo", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#af8235c8c3045cd6bc921004d243ebbd0", null ],
    [ "TestDeleteTelefon", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#ab959479748f014d11a246e8115b2a915", null ],
    [ "TestDeleteTelephely", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a8098b63a2220a28cdc5ba069786f0df4", null ],
    [ "TestDeleteVasarlas", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a21505cebe67030e386e6b68b17067a37", null ],
    [ "TestDeleteVasarlo", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a6799d05d5d95b2f959d0529342d1dffd", null ],
    [ "TestLeltar", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#aac2298f27116b8910faa7899ee4473e3", null ],
    [ "TestReadTelefon", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a0f7c70de2e1668ea829f078dd850a21c", null ],
    [ "TestReadTelephely", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#af308c1db4c76fdd3d44a7802f44f2dc4", null ],
    [ "TestReadVasarlas", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a9f663db7cc37dbce338b73c84688f4b4", null ],
    [ "TestReadVasarlo", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#ad3c36459bffaa8ab1af0ef42aaa82e0b", null ],
    [ "TestSzuresMarkara", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#ac6b220823f16f8e9b95898f75ab64112", null ],
    [ "TestUpdateTelefon", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a8623ceb75874e07f700eeb65669dead5", null ],
    [ "TestUpdateTelephely", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a35cb29099ffe157c0c9c62f1478e6bb0", null ],
    [ "TestUpdateVasarlo", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a360bdb246fe175cd5443c2914fb76703", null ],
    [ "TestVasarlasiAdatok", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html#a99f67c3769ab2d76ebd0862025da27a3", null ]
];