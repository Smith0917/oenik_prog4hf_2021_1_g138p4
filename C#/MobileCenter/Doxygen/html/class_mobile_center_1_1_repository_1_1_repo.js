var class_mobile_center_1_1_repository_1_1_repo =
[
    [ "Repo", "class_mobile_center_1_1_repository_1_1_repo.html#a404a4be4d48a46919ec2b563b3bb65a2", null ],
    [ "Repo", "class_mobile_center_1_1_repository_1_1_repo.html#a37289cdb267d831315d42b23e02eacc3", null ],
    [ "CreateTelefon", "class_mobile_center_1_1_repository_1_1_repo.html#a23044dc90d6916fcd16a1ea774d32d84", null ],
    [ "CreateTelephely", "class_mobile_center_1_1_repository_1_1_repo.html#a20719f298f22e38be326f575af7c6fcf", null ],
    [ "CreateVasarlas", "class_mobile_center_1_1_repository_1_1_repo.html#ae069a7ff5c8c357b0c91fff79ae986f2", null ],
    [ "CreateVasarlo", "class_mobile_center_1_1_repository_1_1_repo.html#af7b82c90071de1abfaae46e4ce280af5", null ],
    [ "DeleteTelefon", "class_mobile_center_1_1_repository_1_1_repo.html#a2526bc85b34e2e217e5553a95c41cb7b", null ],
    [ "DeleteTelephely", "class_mobile_center_1_1_repository_1_1_repo.html#a8475c51bd2eeb378df053955024ba0b9", null ],
    [ "DeleteVasarlas", "class_mobile_center_1_1_repository_1_1_repo.html#a3ac13d9f36397fb7cb553f7beb945457", null ],
    [ "DeleteVasarlo", "class_mobile_center_1_1_repository_1_1_repo.html#a8f8a67d5fb0581957a3acea504d02b7f", null ],
    [ "ReadTelefon", "class_mobile_center_1_1_repository_1_1_repo.html#a4abf6b9b4e5fb7a3a16d23124d9423bd", null ],
    [ "ReadTelephely", "class_mobile_center_1_1_repository_1_1_repo.html#a8cc411777afc03313e60784e5032b307", null ],
    [ "ReadVasarlas", "class_mobile_center_1_1_repository_1_1_repo.html#a249f9cf46e3556ec301862300198390e", null ],
    [ "ReadVasarlo", "class_mobile_center_1_1_repository_1_1_repo.html#aa9fe120f59395d02ab26f067be153b01", null ],
    [ "UpdateTelefon", "class_mobile_center_1_1_repository_1_1_repo.html#a25a3fa1001832c72611b8df07fa0e204", null ],
    [ "UpdateTelephely", "class_mobile_center_1_1_repository_1_1_repo.html#ad98beb2bdc555cd1cf1c2a991fa4eb0b", null ],
    [ "UpdateVasarlo", "class_mobile_center_1_1_repository_1_1_repo.html#a83fa2dedf6d3ca46915fa00cb7d19a55", null ],
    [ "Database", "class_mobile_center_1_1_repository_1_1_repo.html#ac43a566eb9c8d387120d1ef546d13c68", null ]
];