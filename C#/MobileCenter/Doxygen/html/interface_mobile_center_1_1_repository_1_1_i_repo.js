var interface_mobile_center_1_1_repository_1_1_i_repo =
[
    [ "CreateTelefon", "interface_mobile_center_1_1_repository_1_1_i_repo.html#ad3086db83c55b79c8fe7081161197a68", null ],
    [ "CreateTelephely", "interface_mobile_center_1_1_repository_1_1_i_repo.html#a97c83b7e9b34fa3e9e63c6ee9adfc6b2", null ],
    [ "CreateVasarlas", "interface_mobile_center_1_1_repository_1_1_i_repo.html#aa46c61084f6b5888d452ed6fc984faec", null ],
    [ "CreateVasarlo", "interface_mobile_center_1_1_repository_1_1_i_repo.html#acbe695c9906d804d27be732aba56828e", null ],
    [ "DeleteTelefon", "interface_mobile_center_1_1_repository_1_1_i_repo.html#ac3b8dc687de6bbca09dc964127f51665", null ],
    [ "DeleteTelephely", "interface_mobile_center_1_1_repository_1_1_i_repo.html#a60885ae2ba6eb922bd18eeaa941dca07", null ],
    [ "DeleteVasarlas", "interface_mobile_center_1_1_repository_1_1_i_repo.html#aa373451ff7527445a68acc1062557b49", null ],
    [ "DeleteVasarlo", "interface_mobile_center_1_1_repository_1_1_i_repo.html#ad0103876c5ae6bfc2bd85bab3a633442", null ],
    [ "ReadTelefon", "interface_mobile_center_1_1_repository_1_1_i_repo.html#a195f509ab3e6f690a1fb727abc67bcc0", null ],
    [ "ReadTelephely", "interface_mobile_center_1_1_repository_1_1_i_repo.html#a14a5c274a189882daf4701ce8f2cb442", null ],
    [ "ReadVasarlas", "interface_mobile_center_1_1_repository_1_1_i_repo.html#ab40f7f32f4b510d4a004c2ffaab7b396", null ],
    [ "ReadVasarlo", "interface_mobile_center_1_1_repository_1_1_i_repo.html#a49c0d90b243d08a5bd5d44c0b55f9bf7", null ],
    [ "UpdateTelefon", "interface_mobile_center_1_1_repository_1_1_i_repo.html#a1640bf461558bf3dbcd00baa74477504", null ],
    [ "UpdateTelephely", "interface_mobile_center_1_1_repository_1_1_i_repo.html#a0f90500bab8fd283c34bf83ca0746518", null ],
    [ "UpdateVasarlo", "interface_mobile_center_1_1_repository_1_1_i_repo.html#a0eba6467a10a84e4e7291e50c2bac2d0", null ]
];