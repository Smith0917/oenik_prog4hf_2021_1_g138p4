var interface_mobile_center_1_1_logics_1_1_i_logic =
[
    [ "CreateTelefon", "interface_mobile_center_1_1_logics_1_1_i_logic.html#aec2ed2a34d3fed69af3a07b9d569f0a9", null ],
    [ "CreateTelephely", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a90158508ed134b65c83d4ba3f6c0806f", null ],
    [ "CreateVasarlas", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a40b4d0c506f3ab366106d755e0641485", null ],
    [ "CreateVasarlo", "interface_mobile_center_1_1_logics_1_1_i_logic.html#abe327bbac97541e1b94957cae6bdc035", null ],
    [ "DeleteTelefon", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a1c21b7609cd067a5f86909787857b597", null ],
    [ "DeleteTelephely", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a479e3c4641012eca73fe05ecb7bdf1c0", null ],
    [ "DeleteVasarlas", "interface_mobile_center_1_1_logics_1_1_i_logic.html#add33cfc20677e6f25cfc64f2c15ab521", null ],
    [ "DeleteVasarlo", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a392a88bb1e3467b9f9a156a22c6acacb", null ],
    [ "Leltar", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a364e1db93bc91ec6b0a916acd39e3f62", null ],
    [ "MarkaraSzures", "interface_mobile_center_1_1_logics_1_1_i_logic.html#acc974827701e6d415219960ac4f1707e", null ],
    [ "ReadTelefon", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a5367ed9e84adc687ef74b03797c6be18", null ],
    [ "ReadTelephely", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a3e2dfd487227cd3a12e0997d86655cfd", null ],
    [ "ReadVasarlas", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a9200d263a8eaa2210a77ff977f515e83", null ],
    [ "ReadVasarlo", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a8a9330ed94e6eef9fd330cb5048eaa16", null ],
    [ "UpdateTelefon", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a80f591a9a302aec6d7b77bcd238b578e", null ],
    [ "UpdateTelephely", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a743e4391ef0ecd30a049c70424ed41bc", null ],
    [ "UpdateVasarlo", "interface_mobile_center_1_1_logics_1_1_i_logic.html#abbc5e614b0ced0c98a39836834e60f1c", null ],
    [ "VasarlasiAdatok", "interface_mobile_center_1_1_logics_1_1_i_logic.html#a034f9697fd796c61ca700ceb0c158f3a", null ]
];