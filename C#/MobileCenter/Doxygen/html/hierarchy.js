var hierarchy =
[
    [ "DbContext", null, [
      [ "MobileCenter.Data.MobileCenterDatabaseEntities", "class_mobile_center_1_1_data_1_1_mobile_center_database_entities.html", null ]
    ] ],
    [ "MobileCenter.Logics.ILogic", "interface_mobile_center_1_1_logics_1_1_i_logic.html", [
      [ "MobileCenter.Logics.Logic", "class_mobile_center_1_1_logics_1_1_logic.html", null ]
    ] ],
    [ "MobileCenter.Repository.IRepo", "interface_mobile_center_1_1_repository_1_1_i_repo.html", [
      [ "MobileCenter.Repository.Repo", "class_mobile_center_1_1_repository_1_1_repo.html", null ]
    ] ],
    [ "MobileCenter.Logics.Tests.LogicTests", "class_mobile_center_1_1_logics_1_1_tests_1_1_logic_tests.html", null ],
    [ "ConsoleApp1.Program", "class_console_app1_1_1_program.html", null ],
    [ "MobileCenter.Data.Telefon", "class_mobile_center_1_1_data_1_1_telefon.html", null ],
    [ "MobileCenter.Data.Telephely", "class_mobile_center_1_1_data_1_1_telephely.html", null ],
    [ "MobileCenter.Data.Vasarlas", "class_mobile_center_1_1_data_1_1_vasarlas.html", null ],
    [ "MobileCenter.Data.Vasarlo", "class_mobile_center_1_1_data_1_1_vasarlo.html", null ]
];