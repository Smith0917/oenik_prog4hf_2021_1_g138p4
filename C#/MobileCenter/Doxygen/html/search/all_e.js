var searchData=
[
  ['vasarlas',['Vasarlas',['../class_mobile_center_1_1_data_1_1_vasarlas.html',1,'MobileCenter.Data.Vasarlas'],['../class_mobile_center_1_1_data_1_1_mobile_center_database_entities.html#af07e88f1ba4a899208732fc164255893',1,'MobileCenter.Data.MobileCenterDatabaseEntities.Vasarlas()'],['../class_mobile_center_1_1_data_1_1_telefon.html#a059020ce85679709372e5fae82eb8e46',1,'MobileCenter.Data.Telefon.Vasarlas()'],['../class_mobile_center_1_1_data_1_1_telephely.html#ad9c6a8c077d3e03b2bb257f52a7f1a67',1,'MobileCenter.Data.Telephely.Vasarlas()'],['../class_mobile_center_1_1_data_1_1_vasarlo.html#abceac6ece3d745b647df069871e035eb',1,'MobileCenter.Data.Vasarlo.Vasarlas()']]],
  ['vasarlasi_5fdatum',['Vasarlasi_datum',['../class_mobile_center_1_1_data_1_1_vasarlas.html#a824ea03d81ed0341686bfd8837b9141f',1,'MobileCenter::Data::Vasarlas']]],
  ['vasarlasiadatok',['VasarlasiAdatok',['../interface_mobile_center_1_1_logics_1_1_i_logic.html#a034f9697fd796c61ca700ceb0c158f3a',1,'MobileCenter.Logics.ILogic.VasarlasiAdatok()'],['../class_mobile_center_1_1_logics_1_1_logic.html#acce95664b03e8060633e6725b28ca0ef',1,'MobileCenter.Logics.Logic.VasarlasiAdatok()']]],
  ['vasarlasid',['VasarlasID',['../class_mobile_center_1_1_data_1_1_vasarlas.html#a4a6378ddb133837444a8b60353922782',1,'MobileCenter::Data::Vasarlas']]],
  ['vasarlo',['Vasarlo',['../class_mobile_center_1_1_data_1_1_vasarlo.html',1,'MobileCenter.Data.Vasarlo'],['../class_mobile_center_1_1_data_1_1_vasarlas.html#ae3cf0df7337eedadadf7b8e861d6c947',1,'MobileCenter.Data.Vasarlas.Vasarlo()'],['../class_mobile_center_1_1_data_1_1_vasarlo.html#a745539f23b571e78ec5ace84415299aa',1,'MobileCenter.Data.Vasarlo.Vasarlo()']]],
  ['vasarloes',['Vasarloes',['../class_mobile_center_1_1_data_1_1_mobile_center_database_entities.html#a64546d26d502821096726ca1b1b2803f',1,'MobileCenter::Data::MobileCenterDatabaseEntities']]],
  ['vcim',['Vcim',['../class_mobile_center_1_1_data_1_1_vasarlo.html#af2ea7453eeb8f0e8e0aa7a99e68c5130',1,'MobileCenter::Data::Vasarlo']]],
  ['vnev',['Vnev',['../class_mobile_center_1_1_data_1_1_vasarlo.html#a68ecf88694f01054b943f46744858729',1,'MobileCenter::Data::Vasarlo']]]
];
