var searchData=
[
  ['data',['Data',['../namespace_mobile_center_1_1_data.html',1,'MobileCenter']]],
  ['logics',['Logics',['../namespace_mobile_center_1_1_logics.html',1,'MobileCenter']]],
  ['marka',['Marka',['../class_mobile_center_1_1_data_1_1_telefon.html#a7c1882f83722c5b2614499534c50bbe5',1,'MobileCenter::Data::Telefon']]],
  ['markaraszures',['MarkaraSzures',['../interface_mobile_center_1_1_logics_1_1_i_logic.html#acc974827701e6d415219960ac4f1707e',1,'MobileCenter.Logics.ILogic.MarkaraSzures()'],['../class_mobile_center_1_1_logics_1_1_logic.html#ae1ec21c41e6801b8f37f2e7d0733afbd',1,'MobileCenter.Logics.Logic.MarkaraSzures()']]],
  ['mobilecenter',['MobileCenter',['../namespace_mobile_center.html',1,'']]],
  ['mobilecenterdatabaseentities',['MobileCenterDatabaseEntities',['../class_mobile_center_1_1_data_1_1_mobile_center_database_entities.html',1,'MobileCenter.Data.MobileCenterDatabaseEntities'],['../class_mobile_center_1_1_data_1_1_mobile_center_database_entities.html#a7730a708c8e0d8e12cc5dff72376633d',1,'MobileCenter.Data.MobileCenterDatabaseEntities.MobileCenterDatabaseEntities()']]],
  ['repository',['Repository',['../namespace_mobile_center_1_1_repository.html',1,'MobileCenter']]],
  ['tests',['Tests',['../namespace_mobile_center_1_1_logics_1_1_tests.html',1,'MobileCenter::Logics']]]
];
