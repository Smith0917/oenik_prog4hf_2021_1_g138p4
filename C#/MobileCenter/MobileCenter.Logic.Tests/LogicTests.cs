﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MobileCenter.Logics.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using MobileCenter.Data;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Testing class.
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        /// <summary>
        /// Tests the ReadTelefon method.
        /// </summary>
        [Test]
        public void TestReadTelefon()
        {
            var data = new List<Telefon>
            {
                new Telefon { TelefonID = 100, Marka = "Márka1", Tipus = "Típus1", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 101, Marka = "Márka2", Tipus = "Típus2", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 102, Marka = "Márka3", Tipus = "Típus3", Ar = 1000, Tnev = "Buda GSM" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Telefon>>();
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telefons).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            var telefonok = service.ReadTelefon();

            Assert.AreEqual(3, telefonok.Count());
            Assert.AreEqual(100, telefonok[0].TelefonID);
            Assert.AreEqual(101, telefonok[1].TelefonID);
            Assert.AreEqual(102, telefonok[2].TelefonID);
        }

        /// <summary>
        /// Tests the ReadTelephely method.
        /// </summary>
        [Test]
        public void TestReadTelephely()
        {
            var data = new List<Telephely>
            {
              new Telephely { Tnev = "gsm1", Tcim = "cím1", Uzemelteto = "főnök1" },
              new Telephely { Tnev = "gsm2", Tcim = "cím2", Uzemelteto = "főnök2" },
              new Telephely { Tnev = "gsm3", Tcim = "cím3", Uzemelteto = "főnök3" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Telephely>>();
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telephelies).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            var telephelyek = service.ReadTelephely();

            Assert.AreEqual(3, telephelyek.Count());
            Assert.AreEqual("gsm1", telephelyek[0].Tnev);
            Assert.AreEqual("gsm2", telephelyek[1].Tnev);
            Assert.AreEqual("gsm3", telephelyek[2].Tnev);
        }

        /// <summary>
        /// Tests the ReadVasarlo method.
        /// </summary>
        [Test]
        public void TestReadVasarlo()
        {
            var data = new List<Vasarlo>
            {
                new Vasarlo { Telefonszam = 00000000000, Vnev = "tesztNév1", Szul_datum = DateTime.Now, email = "tesztEmail1", Vcim = "tesztCím1" },
                new Vasarlo { Telefonszam = 00000000001, Vnev = "tesztNév2", Szul_datum = DateTime.Now, email = "tesztEmail2", Vcim = "tesztCím2" },
                new Vasarlo { Telefonszam = 00000000002, Vnev = "tesztNév3", Szul_datum = DateTime.Now, email = "tesztEmail3", Vcim = "tesztCím3" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Vasarlo>>();
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Vasarloes).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            var vasarilovak = service.ReadVasarlo();

            Assert.AreEqual(3, vasarilovak.Count());
            Assert.AreEqual(00000000000, vasarilovak[0].Telefonszam);
            Assert.AreEqual(00000000001, vasarilovak[1].Telefonszam);
            Assert.AreEqual(00000000002, vasarilovak[2].Telefonszam);
        }

        /// <summary>
        /// Tests the ReadVasarlas method.
        /// </summary>
        [Test]
        public void TestReadVasarlas()
        {
            var data = new List<Vasarlas>
            {
                new Vasarlas { VasarlasID = 97, Telefonszam = 06303879305, TelefonID = 40, Vasarlasi_datum = DateTime.Now },
                new Vasarlas { VasarlasID = 98, Telefonszam = 06303879305, TelefonID = 41, Vasarlasi_datum = DateTime.Now },
                new Vasarlas { VasarlasID = 99, Telefonszam = 06303879305, TelefonID = 42, Vasarlasi_datum = DateTime.Now },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Vasarlas>>();
            mockSet.As<IQueryable<Vasarlas>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Vasarlas>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Vasarlas>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Vasarlas>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Vasarlas).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            var vasarilovak = service.ReadVasarlas();

            Assert.AreEqual(3, vasarilovak.Count());
            Assert.AreEqual(97, vasarilovak[0].VasarlasID);
            Assert.AreEqual(98, vasarilovak[1].VasarlasID);
            Assert.AreEqual(99, vasarilovak[2].VasarlasID);
        }

        /// <summary>
        /// Tests the CreateTelefon method.
        /// </summary>
        [Test]
        public void TestCreateTelefon()
        {
            var mockSet = new Mock<DbSet<Telefon>>();
            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telefons).Returns(mockSet.Object);
            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new MobileCenter.Logics.Logic(mockRepo.Object);
            service.CreateTelefon(69, "Nokia", "Lumia 500", 40000, "Buda GSM");
            mockSet.Verify(x => x.Add(It.IsAny<Telefon>()), Times.Once());

            mockContext.Verify(x => x.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the CreateTelephely method.
        /// </summary>
        [Test]
        public void TestCreateTelephely()
        {
            var mockSet = new Mock<DbSet<Telephely>>();
            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telephelies).Returns(mockSet.Object);
            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new MobileCenter.Logics.Logic(mockRepo.Object);
            service.CreateTelephely("TesztGSM", "TesztCím", "TesztÜzemeltető");
            mockSet.Verify(x => x.Add(It.IsAny<Telephely>()), Times.Once());

            mockContext.Verify(x => x.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the CreateVasarlo method.
        /// </summary>
        [Test]
        public void TestCreateVasarlo()
        {
            var mockSet = new Mock<DbSet<Vasarlo>>();
            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Vasarloes).Returns(mockSet.Object);
            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new MobileCenter.Logics.Logic(mockRepo.Object);
            service.CreateVasarlo(00000000000, "TesztNév", DateTime.Now, "tesztEmail", "Teszt utca 15.");
            mockSet.Verify(x => x.Add(It.IsAny<Vasarlo>()), Times.Once());

            mockContext.Verify(x => x.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the CreateVasarlas method.
        /// </summary>
        [Test]
        public void TestCreateVasarlas()
        {
            var mockSet = new Mock<DbSet<Vasarlas>>();
            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Vasarlas).Returns(mockSet.Object);
            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new MobileCenter.Logics.Logic(mockRepo.Object);

            service.CreateVasarlas(97, 06303879305, "Buda GSM", 40, DateTime.Now);
            mockSet.Verify(x => x.Add(It.IsAny<Vasarlas>()), Times.Once());

            mockContext.Verify(x => x.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the DeleteTelefon method.
        /// </summary>
        [Test]
        public void TestDeleteTelefon()
        {
            var data = new List<Telefon>
            {
                new Telefon { TelefonID = 100, Marka = "Márka1", Tipus = "Típus1", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 101, Marka = "Márka2", Tipus = "Típus2", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 102, Marka = "Márka3", Tipus = "Típus3", Ar = 1000, Tnev = "Buda GSM" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Telefon>>();
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telefons).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            service.DeleteTelefon(100);

            mockSet.Verify(m => m.Remove(It.IsAny<Telefon>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the DeleteTelephely method.
        /// </summary>
        [Test]
        public void TestDeleteTelephely()
        {
            var data = new List<Telephely>
            {
                new Telephely { Tnev = "tesztNév1", Tcim = "tesztCím1", Uzemelteto = "tester1" },
                new Telephely { Tnev = "tesztNév3", Tcim = "tesztCím2", Uzemelteto = "tester2" },
                new Telephely { Tnev = "tesztNév3", Tcim = "tesztCím3", Uzemelteto = "tester3" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Telephely>>();
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telephelies).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            service.DeleteTelephely("tesztNév1");

            mockSet.Verify(m => m.Remove(It.IsAny<Telephely>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the DeleteVasarlo method.
        /// </summary>
        [Test]
        public void TestDeleteVasarlo()
        {
            var data = new List<Vasarlo>
            {
                new Vasarlo { Telefonszam = 00000000000, Vnev = "tesztNév1", Szul_datum = DateTime.Now, email = "tesztEmail1", Vcim = "tesztCím1" },
                new Vasarlo { Telefonszam = 00000000001, Vnev = "tesztNév2", Szul_datum = DateTime.Now, email = "tesztEmail2", Vcim = "tesztCím2" },
                new Vasarlo { Telefonszam = 00000000002, Vnev = "tesztNév3", Szul_datum = DateTime.Now, email = "tesztEmail3", Vcim = "tesztCím3" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Vasarlo>>();
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Vasarloes).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            service.DeleteVasarlo(00000000000);

            mockSet.Verify(m => m.Remove(It.IsAny<Vasarlo>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the DeleteVasarlas methpd.
        /// </summary>
        [Test]
        public void TestDeleteVasarlas()
        {
            var data = new List<Vasarlas>
            {
                 new Vasarlas { VasarlasID = 97, Telefonszam = 06303879305, TelefonID = 40, Vasarlasi_datum = DateTime.Now },
                 new Vasarlas { VasarlasID = 98, Telefonszam = 06303879305, TelefonID = 41, Vasarlasi_datum = DateTime.Now },
                 new Vasarlas { VasarlasID = 99, Telefonszam = 06303879305, TelefonID = 42, Vasarlasi_datum = DateTime.Now },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Vasarlas>>();
            mockSet.As<IQueryable<Vasarlas>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Vasarlas>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Vasarlas>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Vasarlas>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Vasarlas).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            service.DeleteVasarlas(99);

            mockSet.Verify(m => m.Remove(It.IsAny<Vasarlas>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the UpdateTelefon method.
        /// </summary>
        [Test]
        public void TestUpdateTelefon()
        {
            var data = new List<Telefon>
            {
                new Telefon { TelefonID = 100, Marka = "Márka1", Tipus = "Típus1", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 101, Marka = "Márka2", Tipus = "Típus2", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 102, Marka = "Márka3", Tipus = "Típus3", Ar = 1000, Tnev = "Buda GSM" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Telefon>>();
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telefons).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            service.UpdateTelefon(100, "UjMarka", "UjTipus", 200, "Tirpák GSM");

            var telefon = mockContext.Object.Telefons.ToList().Where(x => x.TelefonID == 100).First();

            Assert.AreEqual(100, telefon.TelefonID);
            Assert.AreEqual("UjMarka", telefon.Marka);
            Assert.AreEqual("UjTipus", telefon.Tipus);
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the UpdateTelephely method.
        /// </summary>
        [Test]
        public void TestUpdateTelephely()
        {
            var data = new List<Telephely>
            {
              new Telephely { Tnev = "gsm1", Tcim = "cím1", Uzemelteto = "főnök1" },
              new Telephely { Tnev = "gsm2", Tcim = "cím2", Uzemelteto = "főnök2" },
              new Telephely { Tnev = "gsm3", Tcim = "cím3", Uzemelteto = "főnök3" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Telephely>>();
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Telephely>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telephelies).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            service.UpdateTelephely("gsm1", "test", "tester");

            var telephely = mockContext.Object.Telephelies.ToList().Where(x => x.Tnev == "gsm1").First();

            Assert.AreEqual("gsm1", telephely.Tnev);
            Assert.AreEqual("test", telephely.Tcim);
            Assert.AreEqual("tester", telephely.Uzemelteto);
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the UpdateVasarlo method.
        /// </summary>
        [Test]
        public void TestUpdateVasarlo()
        {
            var data = new List<Vasarlo>
            {
              new Vasarlo { Telefonszam = 00000000000, Vnev = "tesztNév1", Szul_datum = DateTime.Now, email = "tesztEmail1", Vcim = "tesztCím1" },
              new Vasarlo { Telefonszam = 00000000001, Vnev = "tesztNév2", Szul_datum = DateTime.Now, email = "tesztEmail2", Vcim = "tesztCím2" },
              new Vasarlo { Telefonszam = 00000000002, Vnev = "tesztNév3", Szul_datum = DateTime.Now, email = "tesztEmail3", Vcim = "tesztCím3" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Vasarlo>>();
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Vasarlo>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Vasarloes).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            service.UpdateVasarlo(00000000000, "nev", DateTime.Now, "email", "cim");

            var vasarlo = mockContext.Object.Vasarloes.ToList().Where(x => x.Telefonszam == 00000000000).First();

            Assert.AreEqual(00000000000, vasarlo.Telefonszam);
            Assert.AreEqual("nev", vasarlo.Vnev);
            Assert.AreEqual("email", vasarlo.email);
            Assert.AreEqual("cim", vasarlo.Vcim);
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        /// <summary>
        /// Tests the VasarlasiAdatok method.
        /// </summary>
        [Test]
        public void TestVasarlasiAdatok()
        {
            var data1 = new List<Vasarlo>
            {
              new Vasarlo { Telefonszam = 00000000000, Vnev = "tesztNév1", Szul_datum = DateTime.Now, email = "tesztEmail1", Vcim = "tesztCím1" },
              new Vasarlo { Telefonszam = 00000000001, Vnev = "tesztNév2", Szul_datum = DateTime.Now, email = "tesztEmail2", Vcim = "tesztCím2" },
              new Vasarlo { Telefonszam = 00000000002, Vnev = "tesztNév3", Szul_datum = DateTime.Now, email = "tesztEmail3", Vcim = "tesztCím3" },
            }.AsQueryable();

            var mockSet1 = new Mock<DbSet<Vasarlo>>();
            mockSet1.As<IQueryable<Vasarlo>>().Setup(m => m.Provider).Returns(data1.Provider);
            mockSet1.As<IQueryable<Vasarlo>>().Setup(m => m.Expression).Returns(data1.Expression);
            mockSet1.As<IQueryable<Vasarlo>>().Setup(m => m.ElementType).Returns(data1.ElementType);
            mockSet1.As<IQueryable<Vasarlo>>().Setup(m => m.GetEnumerator()).Returns(data1.GetEnumerator());

            var data2 = new List<Telefon>
            {
                new Telefon { TelefonID = 100, Marka = "Márka1", Tipus = "Típus1", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 101, Marka = "Márka2", Tipus = "Típus2", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 102, Marka = "Márka3", Tipus = "Típus3", Ar = 1000, Tnev = "Buda GSM" },
            }.AsQueryable();

            var mockSet2 = new Mock<DbSet<Telefon>>();
            mockSet2.As<IQueryable<Telefon>>().Setup(m => m.Provider).Returns(data2.Provider);
            mockSet2.As<IQueryable<Telefon>>().Setup(m => m.Expression).Returns(data2.Expression);
            mockSet2.As<IQueryable<Telefon>>().Setup(m => m.ElementType).Returns(data2.ElementType);
            mockSet2.As<IQueryable<Telefon>>().Setup(m => m.GetEnumerator()).Returns(data2.GetEnumerator());

            var data3 = new List<Vasarlas>
            {
                new Vasarlas { VasarlasID = 97, Telefonszam = 00000000001, TelefonID = 100, Vasarlasi_datum = DateTime.Now },
                new Vasarlas { VasarlasID = 98, Telefonszam = 00000000001, TelefonID = 101, Vasarlasi_datum = DateTime.Now },
                new Vasarlas { VasarlasID = 99, Telefonszam = 00000000001, TelefonID = 102, Vasarlasi_datum = DateTime.Now },
            }.AsQueryable();

            var mockSet3 = new Mock<DbSet<Vasarlas>>();
            mockSet3.As<IQueryable<Vasarlas>>().Setup(m => m.Provider).Returns(data3.Provider);
            mockSet3.As<IQueryable<Vasarlas>>().Setup(m => m.Expression).Returns(data3.Expression);
            mockSet3.As<IQueryable<Vasarlas>>().Setup(m => m.ElementType).Returns(data3.ElementType);
            mockSet3.As<IQueryable<Vasarlas>>().Setup(m => m.GetEnumerator()).Returns(data3.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Vasarloes).Returns(mockSet1.Object);
            mockContext.Setup(x => x.Telefons).Returns(mockSet2.Object);
            mockContext.Setup(x => x.Vasarlas).Returns(mockSet3.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            string teszt = service.VasarlasiAdatok(97);

            Assert.AreEqual("tesztNév2 vásárolt Márka1 Típus1 készüléket 1000-ért.", teszt);
        }

        /// <summary>
        /// Tests the SzuresMarkara method.
        /// </summary>
        [Test]
        public void TestSzuresMarkara()
        {
            var data = new List<Telefon>
            {
                new Telefon { TelefonID = 100, Marka = "Márka1", Tipus = "Típus1", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 101, Marka = "Márka2", Tipus = "Típus2", Ar = 1000, Tnev = "Buda GSM" },
                new Telefon { TelefonID = 102, Marka = "Márka3", Tipus = "Típus3", Ar = 1000, Tnev = "Buda GSM" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Telefon>>();
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Telefon>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telefons).Returns(mockSet.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            List<string> lista = new List<string>();
            lista = service.MarkaraSzures("Márka1");

            Assert.AreEqual(1, lista.Count());
            Assert.AreEqual("ID: 100 Modell: Márka1 Típus1 Ára: 1000 itt: Buda GSM", lista[0]);
        }

        /// <summary>
        /// Tests the Leltar method.
        /// </summary>
        [Test]
        public void TestLeltar()
        {
            var data1 = new List<Telefon>
            {
                new Telefon { TelefonID = 100, Marka = "Márka1", Tipus = "Típus1", Ar = 1000, Tnev = "gsm1" },
                new Telefon { TelefonID = 101, Marka = "Márka2", Tipus = "Típus2", Ar = 1000, Tnev = "gsm1" },
                new Telefon { TelefonID = 102, Marka = "Márka3", Tipus = "Típus3", Ar = 1000, Tnev = "gsm2" },
            }.AsQueryable();

            var mockSet1 = new Mock<DbSet<Telefon>>();
            mockSet1.As<IQueryable<Telefon>>().Setup(m => m.Provider).Returns(data1.Provider);
            mockSet1.As<IQueryable<Telefon>>().Setup(m => m.Expression).Returns(data1.Expression);
            mockSet1.As<IQueryable<Telefon>>().Setup(m => m.ElementType).Returns(data1.ElementType);
            mockSet1.As<IQueryable<Telefon>>().Setup(m => m.GetEnumerator()).Returns(data1.GetEnumerator());

            var data2 = new List<Telephely>
            {
              new Telephely { Tnev = "gsm1", Tcim = "cím1", Uzemelteto = "főnök1" },
              new Telephely { Tnev = "gsm2", Tcim = "cím2", Uzemelteto = "főnök2" },
              new Telephely { Tnev = "gsm3", Tcim = "cím3", Uzemelteto = "főnök3" },
            }.AsQueryable();

            var mockSet2 = new Mock<DbSet<Telephely>>();
            mockSet2.As<IQueryable<Telephely>>().Setup(m => m.Provider).Returns(data2.Provider);
            mockSet2.As<IQueryable<Telephely>>().Setup(m => m.Expression).Returns(data2.Expression);
            mockSet2.As<IQueryable<Telephely>>().Setup(m => m.ElementType).Returns(data2.ElementType);
            mockSet2.As<IQueryable<Telephely>>().Setup(m => m.GetEnumerator()).Returns(data2.GetEnumerator());

            var mockContext = new Mock<MobileCenterDatabaseEntities>();
            mockContext.Setup(x => x.Telefons).Returns(mockSet1.Object);
            mockContext.Setup(x => x.Telephelies).Returns(mockSet2.Object);

            var mockRepo = new Mock<Repository.Repo>(mockContext.Object);
            var service = new Logic(mockRepo.Object);

            List<string> teszt = service.Leltar("gsm1");

            Assert.That(teszt.Count() == 3);
            Assert.AreEqual("Azonosító      Márka          Típus          Ár             ", teszt[0]);
            Assert.AreEqual("100            Márka1         Típus1         1000", teszt[1]);
            Assert.AreEqual("101            Márka2         Típus2         1000", teszt[2]);
        }
    }
}
