/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modell;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Smith
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Telefonok {
    
    private static final Random RANDOM = new Random();
    

    @XmlElement
    static List<Telefon> telefon;

    public Telefonok() {
        this.telefon = new ArrayList<>();
        for (int i = 0; i < 15; i++){
            GenerateTelefon();
        }
        
    }

    public List<Telefon> getTelefonok() {
        return telefon;
    }
    
    private static void GenerateTelefon()
    {
        int ID = RANDOM.nextInt(500) + 100;
        String marka = generateBrand();
        String tipus = generateType(marka);
        int ar = RANDOM.nextInt(200000) + 30000;
        String tnev = generateDepartment();
        telefon.add(new Telefon(ID, marka, tipus, ar, tnev));
    }
    
   
    
public static String generateDepartment()
{
int temp = RANDOM.nextInt(4);
if (temp == 0)
{
return "Pesti GSM";
}
else if (temp == 1){
return "Buda GSM";
}
else if (temp == 2){
    return "Vidám GSM";}
else if (temp == 3){
    return "Szlambathely GSM";}
else return "Tirpák GSM";
}

public static String generateBrand()
{
int temp = RANDOM.nextInt(5);
if (temp == 0)
{
return "Sony";
}
else if (temp == 1){
return "Huawei";
}
else if (temp == 2){
    return "Honor";}
else if (temp == 3){
    return "Nokia";}
else if (temp == 4){
    return "Samsung";}
else return "Apple";
}

public static String generateType(String brand)
{
    int temp = RANDOM.nextInt(5);
if (brand == "Sony"){

if (temp == 0)
{
return "Xperia XZ3";
}
else if (temp == 1){
return "XA2 Plus";
}
else if (temp == 2){
    return "L2";}
else if (temp == 3){
    return "Z1";}
else if (temp == 4){
    return "Z3+";}
else return "X Compact";
} else if (brand == "Huawei"){
    if (temp == 0)
{
return "P10";
}
else if (temp == 1){
return "P20 Lite";
}
else if (temp == 2){
    return "P20";}
else if (temp == 3){
    return "P20 Pro";}
else if (temp == 4){
    return "Mate 10";}
else return "P9";
}else if (brand == "Honor"){
    if (temp == 0)
{
return "7";
}
else if (temp == 1){
return "8";
}
else if (temp == 2){
    return "9";}

else return "10";
}else if (brand == "Nokia"){
    if (temp == 0)
{
return "106";
}
else if (temp == 1){
return "7.1";
}
else if (temp == 2){
    return "X7";}
else if (temp == 3){
    return "8 Sirocco";}
else if (temp == 4){
    return "3310 4G";}
else return "8";
}else if (brand == "Samsung"){
    if (temp == 0)
{
return "Galaxy Note9";
}
else if (temp == 1){
return "Galaxy A6s";
}
else if (temp == 2){
    return "Galaxy J6+";}
else if (temp == 3){
    return "Galaxy On6";}
else if (temp == 4){
    return "Galaxy A8s";}
else return "Galaxy J3 (2018)";
}else {
    if (temp == 0)
{
return "iPhone 7";
}
else if (temp == 1){
return "iPhone 8";
}
else if (temp == 2){
    return "iPhone X";}
else if (temp == 3){
    return "iPhone XS";}
else if (temp == 4){
    return "iPhone XR";}
else return "iPad Pro 11";
}
}
}
