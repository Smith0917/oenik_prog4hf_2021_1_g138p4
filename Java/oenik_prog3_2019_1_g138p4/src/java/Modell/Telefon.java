/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modell;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author smith
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Telefon {
    int ID;
    String marka;
    String tipus;
    int ar;
    String tnev;

    public Telefon(int ID, String marka, String tipus, int ar, String tnev) {
        this.ID = ID;
        this.marka = marka;
        this.tipus = tipus;
        this.ar = ar;
        this.tnev = tnev;
    }

    public Telefon() {}
    
    
    
}
